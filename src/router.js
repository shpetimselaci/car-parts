import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/services',
      name: 'services',
      component: () => import(/* webpackChunkName: "services" */ './views/Service.vue')
    },
    {
      path: '/vision',
      name: 'vision',
      component: () => import(/* webpackChunkName: "vision" */ './views/Vision.vue')
    },
    {
      path: '/mission',
      name: 'mission',
      component: () => import(/* webpackChunkName: "mission" */ './views/Mission.vue')
    },
    {
      path: '/thankyou',
      name: 'thankyou',
      component: () => import(/* webpackChunkName: "thankyou" */ './views/ThankYou.vue')
    },
    {
      path: '/parts',
      name: 'parts',
      component: () => import(/* webpackChunkName: "parts" */ './views/Parts.vue')
    },
    { 
      path: '*',
      redirect: '/'
    },
  ]
})
