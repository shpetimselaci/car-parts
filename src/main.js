import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'popper.js';
import 'bootstrap';
import 'jquery';
import Aos from 'aos';

import 'aos/dist/aos.css'
import './assets/main.css'
import router from './router'
import './registerServiceWorker'
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)

Vue.config.productionTip = false

export const bus = new Vue();

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');

Aos.init();
