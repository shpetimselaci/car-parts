const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors =require("cors");
const nodemailer =require('nodemailer');
const { createTransport } = nodemailer;
const {
    google
} =require('googleapis');
require('dotenv').config()

const OAuth2 = google.auth.OAuth2;
const app = express();



app.use(bodyParser.json({
    "limit": "50mb"
}));
app.use(bodyParser.urlencoded({
    "limit": "50mb",
    "extended": true
}));
app.use(compression());
app.use(express.json());
const corsOptions = {
    origin: 'http://localhost:8080',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

app.post('/send', async (req, res) => {
    try {
        const request = req.body;
        const authClient = new OAuth2({
            clientId: process.env.CLIENT_ID,
            clientSecret: process.env.CLIENT_SECRET,
        });
        authClient.setCredentials({
            refresh_token: process.env.REFRESH_TOKEN,
        });
        const {
            token: accessToken
        } = await authClient.getAccessToken();

        const output = `
    <h1>You have a new request from ${request.Name}</h1>
    <h3>Contact Details:</h3>
    <ul>  
    <li>Name: ${request.Name}</li>
    <li>Email: ${request.Email}</li>
    <li>Make: ${request.Make}</li>
    <li>Model: ${request.Model}</li>
    <li>Vin number: ${request.VinNum}</li>
    <li>Production Date: ${request.ProductionDate}</li>
    <li>Parts: ${request.Parts}</li>
    <li>Phone: ${request.PhoneNumber}</li>
    </ul>
    <h3>Message</h3>
    <p>${request.Message}</p>
`;
        const auth = {
            user: 'caribbeanautopartshunter@gmail.com',
            type: 'OAuth2',
            clientId: process.env.CLIENT_ID,
            clientSecret: process.env.CLIENT_SECRET,
            refreshToken: process.env.REFRESH_TOKEN,
            accessToken
        };
        let transporter = createTransport({
            service: 'gmail',
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth,
        });

        await new Promise((resolve, reject) => {
            transporter.sendMail({
                from: '${request.Email}',
                to: 'caribbeanautopartshunter@gmail.com',
                subject: `Part Request from ${request.Name}`,
                text: `${request.Name} wants to get the following part(s):`,
                html: output,
                attachments: request.Images,
            }, (error) => {
                if (error) {
                    console.log('error', error);
                    reject(error);
                } else {
                    resolve('ok');
                }
            })
        });
        res.status(200).json({ message: 'Sent successfully' });
        // send mail with defined transport object
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Something went wrong' });
    }
});

app.listen(80, () => {
    console.log('Listening on port 80');
});
