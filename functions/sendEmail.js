import { createTransport } from 'nodemailer';
import { google } from 'googleapis';
const OAuth2 = google.auth.OAuth2;

const headers = {
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': '*'
};
const data = {
  CLIENT_ID: "604191731540-r594m5orc0stv8cl9hf3rb9g9jff2egq.apps.googleusercontent.com",
CLIENT_SECRET: "snrXYslq53dA3PsTXE5XBOKN",
REFRESH_TOKEN: "1//04e0vw2BFr0m7CgYIARAAGAQSNwF-L9IrGLE06C-WqorOz5BY_927Tcw9W6S-kKcO0ofLtMl6k8z_cRS40I2E-p8znIRgjPjCm58"
}
export async function handler(event) {


  try {
    const authClient = new OAuth2({
      clientId: process.env.CLIENT_ID || data.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET || data.CLIENT_SECRET,
    });
    authClient.setCredentials({
      refresh_token: process.env.REFRESH_TOKEN || data.REFRESH_TOKEN,
    });
    const {token: accessToken} = await authClient.getAccessToken();
  
    let request = event.body;
    try {
      request = JSON.parse(event.body);
    } catch (error) {
      console.log('error', error);
    }
    const output = `
    <h1>You have a new request from ${request.Name}</h1>
    <h3>Contact Details:</h3>
    <ul>  
      <li>Name: ${request.Name}</li>
      <li>Email: ${request.Email}</li>
      <li>Make: ${request.Make}</li>
      <li>Model: ${request.Model}</li>
      <li>Vin number: ${request.VinNum}</li>
      <li>Production Date: ${request.ProductionDate}</li>
      <li>Parts: ${request.Parts}</li>
      <li>Phone: ${request.PhoneNumber}</li>
    </ul>
    <h3>Message</h3>
    <p>${request.Message}</p>
  `;
  const auth = {
    user: 'caribbeanautopartshunter@gmail.com',
    type: 'OAuth2',
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    refreshToken: process.env.REFRESH_TOKEN,
    accessToken 
  };
  console.log(auth);
    let transporter = createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth,
    });

    await new Promise((resolve, reject) => {
      transporter.sendMail({
        from: '${request.Email}',
        to: 'caribbeanautopartshunter@gmail.com',
        subject: `Part Request from ${request.Name}`,
        text: `${request.Name} wants to get the following part(s):`,
        html: output,
        attachments: request.Images,
      }, (error) => {
        if (error) {
          console.warn('error', error);
          reject(error);
        } else {
          resolve('ok');
        }
      })
    });
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: `Send`
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      headers,
      body: 'Something went wrong!'
    };
  }
}