module.exports = {
  pluginOptions: {
    express: {
      shouldServeApp: true,
      serverDir: './server',
      port: 80
    },
    critical: {
      width: 375,
      height: 565    
    }
  },
  chainWebpack: config => {
    config.plugins
      .delete('split-manifest')
      .delete('inline-manifest')
  },
  devServer: {
    proxy: {
      '/send': {
        target: 'http://localhost:3000',
        ws: true,
        changeOrigin: true
      },
    }
  }
}
